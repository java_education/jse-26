package ru.t1.oskinea.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String DESCRIPTION = "Show developer info.";

    @NotNull
    private static final String NAME = "about";

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[APPLICATION]");
        System.out.println("Name: " + service.getApplicationName());
        System.out.println();

        System.out.println("[AUTHOR]");
        System.out.println("Name: " + service.getAuthorName());
        System.out.println("E-mail: " + service.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("Branch: " + service.getGitBranch());
        System.out.println("Commit Id: " + service.getGitCommitId());
        System.out.println("Committer: " + service.getGitCommitterName());
        System.out.println("E-mail: " + service.getGitCommitterEmail());
        System.out.println("Message: " + service.getGitCommitMessage());
        System.out.println("Time: " + service.getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
