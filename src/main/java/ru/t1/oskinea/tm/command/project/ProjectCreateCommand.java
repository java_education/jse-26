package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Create new project.";

    @NotNull
    private static final String NAME = "project-create";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
