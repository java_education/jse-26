package ru.t1.oskinea.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.repository.IRepository;
import ru.t1.oskinea.tm.api.service.IService;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.exception.entity.EntityNotFoundException;
import ru.t1.oskinea.tm.exception.field.IdEmptyException;
import ru.t1.oskinea.tm.exception.field.IndexIncorrectException;
import ru.t1.oskinea.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {


    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll((Comparator<M>) sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        if (id.isEmpty()) return null;
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
