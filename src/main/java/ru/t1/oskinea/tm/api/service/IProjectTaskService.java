package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Task unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

}
